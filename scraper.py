import requests
from bs4 import BeautifulSoup
import pandas
import yaml

url = 'https://www.timeout.com/newyork/movies/best-movies-of-all-time'
moviesDict = {}
response = requests.get(url)
soup = BeautifulSoup(response.text, 'lxml')
items = soup.find_all('div', class_='articleContent _articleContent_1wubv_197')

for n, item in enumerate(items):
    try:  # два варианта class для itemInfo попадаются на странице, пробуем одно, если не подходит, то другое
        itemInfo = item.find('div', class_='_title_1wubv_9 _titleNoCredit_1wubv_178').text.strip()
    except AttributeError:
        itemInfo = item.find('div', class_='_title_1wubv_9').text.strip()
    finally:
        itemDesc = item.find('div', class_='_summary_1wubv_21').text.strip()
        moviesDict[n] = (itemInfo, itemDesc)

# для группировки будем использовать пакет Pandas
moviesDf = pandas.DataFrame.from_dict(moviesDict)

# транспонируем датафрейм, т.к. при импорте из словаря ключи являются столбцами
moviesDf = moviesDf.transpose()

# присвоим имена столбцам датафрейма
moviesDf.columns = ['Raw information', 'Description']


def extracttitle(row):  # извлечение названия фильма, находящееся между номером позиции в топе и годом выхода
    title = str(row['Raw information'])
    title = title.rpartition('(')[0][:-1]  # заканчивается удалением пробела
    title = title.rpartition('.')[2][1:]  # заканчивается удалением пробела
    return title


# извлечение года выхода, если год не указан (нет скобок как у 53. Blade Runner) то None.
# конечно, это работает только применимо к конкретному рейтингу, где в названиях фильмов отсутствуют скобки.
def extractyear(row):
    title = str(row['Raw information'])
    if title.find('(') == -1:
        return None
    else:
        return title.split('(')[-1].replace(')', '')


moviesDf['Position'] = moviesDf.apply(lambda row: row['Raw information'].split('.')[0], axis=1)
moviesDf['Title'] = moviesDf.apply(extracttitle, axis=1)
moviesDf['Year'] = moviesDf.apply(extractyear, axis=1)

# удаляем лишнюю колонку, из которой были извлечены 'Position', 'Title' и 'Year'
moviesDf = moviesDf.drop(['Raw information'], axis=1)

# выстраиваем колонки к нужном порядке
moviesDf = moviesDf[['Position', 'Title', 'Year', 'Description']]

# индексируем датафрейм по столбцу 'Position'
moviesDf = moviesDf.set_index('Position')

# снимаем ограничение на максимальное количество строк при печати датафрейма в консоли
pandas.set_option('display.max_rows', None)

# Показываем результат скрейпинга:
print(f'\nТОП-{len(moviesDict)} фильмов:\n', moviesDf)

# группировка по году выхода и подсчёт фильмов вышедших за указанный год
groupbyYearDf = moviesDf.groupby('Year')['Title'].nunique().to_frame()
groupbyYearDf.columns = ['Movies amount']

# упорядочим начиная с самых "урожайных" годов
groupbyYearDf = groupbyYearDf.sort_values(by='Movies amount', ascending=False)

print('\nГоды выхода выдающихся фильмов и их количество:')
print(groupbyYearDf)

# для удобного порядка ключей в yaml переименуем колонки датафрейма
moviesDf.index.names = ['1-Position']
moviesDf.columns = ['2-Title', '3-Year', '4-Description']
exportDict = moviesDf.reset_index().to_dict(orient='records')

with open(r'result.yaml', 'w') as file:
    doc = yaml.dump(exportDict, file)

print('\nФайл result.yaml создан в корневой папке.')
